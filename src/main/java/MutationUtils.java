import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

class MutationUtils {

    static Panel findMinimalAreaPanel(List<Panel> panels) {
        return Collections.min(panels, Comparator.comparingInt(Panel::getArea));
    }

    static Panel filterByArea(List<Panel> panels, int neededArea) {
        List<Panel> filteredPanels = panels
                .stream()
                .filter(it -> it.getArea() >= neededArea)
                .collect(Collectors.toList());
        if (filteredPanels.size() == 0) return null;
        return Collections.min(filteredPanels, Comparator.comparingInt(Panel::getArea));
    }

}
